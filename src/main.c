#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "image.h"
#include "ast.h"
#include "parser.h"

#define UNUSED(something) (void)(something)

bool check_readable_p6_ppm_file(const char* const path_file)
{
    FILE* ppm_file_maybe = fopen(path_file, "r");
    if (ppm_file_maybe == NULL)   //pas ouvrable en écriture, il semblerait
        return false;

    bool file_ok = true;
    char p6_header[2];
    const size_t nb_bytes = fread(p6_header, sizeof(char), 2, ppm_file_maybe);
    if (nb_bytes != 2 || p6_header[0] != 'P' || p6_header[1] != '6')
        file_ok = false;

    fclose(ppm_file_maybe);
    return file_ok;
}

void assert_file_readable(const char* const path_file)
{
    FILE* maybe_readable_file = fopen(path_file, "r");
    if (maybe_readable_file == NULL)
    {
        printf("%s ne peut pas être ouvert en lecture\n", path_file);
        exit(1);
    }
    fclose(maybe_readable_file);
}

void assert_file_writable(const char* const path_file)
{
    FILE* maybe_writable_file = fopen(path_file, "w");
    if (maybe_writable_file == NULL)
    {
        printf("%s ne peut pas être ouvert en écriture\n", path_file);
        exit(1);
    }
    fclose(maybe_writable_file);
}

void assert_ppm_file_readable(const char* const path_file)
{
    if (!check_readable_p6_ppm_file(path_file))
    {
        printf("%s ne semble pas être un fichier ppm binaire lisible\n", path_file);
        exit(1);
    }
}

int main(int argc, char** argv)
{
    UNUSED(argc);
    UNUSED(argv);

    if (argc != 3)
    {
        printf("Usage: %s ast_file name_output_file\n", argv[0]);
        return 1;
    }
    const char *triangle_file_name = "motifs/triangle_32.ppm", *carre_file_name = "motifs/carre_32.ppm";
    assert_ppm_file_readable(triangle_file_name);
    assert_ppm_file_readable(carre_file_name);

    const char *ast_file_name = argv[1], *output_file_name = argv[2];

    assert_file_readable(ast_file_name);
    struct noeud_ast* noeud = NULL;
    analyser(ast_file_name, &noeud);
    struct patchwork* patch = noeud->evaluer(noeud);

    assert_file_writable(output_file_name);

    FILE* file_sortie = fopen(output_file_name, "w");
    creer_image(patch, carre_file_name, triangle_file_name, file_sortie);
    fclose(file_sortie);
   
    liberer_expression(noeud);
    liberer_patchwork(patch);
    return 0;
}
