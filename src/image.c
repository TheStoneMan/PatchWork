#include <stdlib.h>
#include <stdio.h>
#include "image.h"
#include "list.h"

#define UNUSED(something) (void)(something)

//Spécialisation de l'interface du type "List" pour le type "char", ce qui génère
//les fonctions "List_char_new", "List_char_delete", etc.
create_list(char)

struct fichier_ppm_data
{
    unsigned int largeur, hauteur;
    List_char* donnees_pixel;  /*Un tableau de taille largeur*hauteur, dont
                                 les couleurs d'un pixel sont stockées successivement.
                                 Pour accéder à la première couuleur d'un pixel situé
                                 en (x,y), il faut accéder à l'élément y*largeur*3+x*3 */
};

void read_ppm_size(FILE* ppm_file, unsigned int* w, unsigned int* h)
{
    unsigned int maximum = 0;
    //On ignore le 'P6'
    fgetc(ppm_file);
    fgetc(ppm_file);

    int ret = fscanf(ppm_file, " %u %u %u ", w, h, &maximum);
    (void)ret;
}

/* Analyse le contenu d'un fichier ppm */
struct fichier_ppm_data lire_fichier_ppm(const char* const chemin_fichier)
{
    struct fichier_ppm_data resultat;
    FILE* file_ppm = fopen(chemin_fichier, "r");
    read_ppm_size(file_ppm, &resultat.largeur, &resultat.hauteur);
    resultat.donnees_pixel = List_char_new();
    //Lecture des pixels
    List_char_resize(resultat.donnees_pixel, resultat.largeur * resultat.hauteur * 3, 0);
    size_t ret = fread(resultat.donnees_pixel->ptrMemory, 1, List_char_size(resultat.donnees_pixel),
            file_ppm);
    (void)ret;

    fclose(file_ppm);
    return resultat;
}

struct fichier_ppm_data rotation_anti_horaire_fichier_ppm(struct fichier_ppm_data original)
{
    struct fichier_ppm_data resultat;
    resultat.largeur = original.hauteur;
    resultat.hauteur = original.largeur;
    resultat.donnees_pixel = List_char_new();
    List_char_resize(resultat.donnees_pixel, List_char_size(original.donnees_pixel), 0);
    //Copier les pixels au bon endroit
    for (size_t y = 0; y < resultat.hauteur; y++)
    {
        for (size_t x = 0; x < resultat.largeur; x++)
        {
            const size_t x_original = resultat.hauteur - y - 1;
            const size_t y_original = x;
            //Calcul des offsets en mémoire de la première couleur du pixel.
            const size_t position_original = x_original * 3 + y_original * original.largeur * 3;
            const size_t position_resultat = x * 3 + y * 3 * resultat.largeur;
            //Recopie des composantes R,G,B
            List_char_set(resultat.donnees_pixel, position_resultat, List_char_get(original.donnees_pixel, position_original));
            List_char_set(resultat.donnees_pixel, position_resultat + 1, List_char_get(original.donnees_pixel, position_original + 1));
            List_char_set(resultat.donnees_pixel, position_resultat + 2, List_char_get(original.donnees_pixel, position_original + 2));
        }
    }
    return resultat;
}

void creer_image(const struct patchwork *patch,
                        const char *fichier_ppm_carre,
                        const char *fichier_ppm_triangle,
		 FILE *fichier_sortie)
{
    struct fichier_ppm_data triangles[4];
    triangles[0] = lire_fichier_ppm(fichier_ppm_triangle);
    triangles[1] = rotation_anti_horaire_fichier_ppm(triangles[0]);
    triangles[2] = rotation_anti_horaire_fichier_ppm(triangles[1]);
    triangles[3] = rotation_anti_horaire_fichier_ppm(triangles[2]);

    struct fichier_ppm_data carres[4];
    carres[0] = lire_fichier_ppm(fichier_ppm_carre);
    carres[1] = rotation_anti_horaire_fichier_ppm(carres[0]);
    carres[2] = rotation_anti_horaire_fichier_ppm(carres[1]);
    carres[3] = rotation_anti_horaire_fichier_ppm(carres[2]);

    //DEBUT DU HEADER
    fputs("P6\n", fichier_sortie);
    char buffer_string[900];   //On prend large
    unsigned int maximum_ppm = 255;
    snprintf(buffer_string, 900, "%u %u %d\n", patch->largeur * triangles[0].largeur, patch->hauteur * triangles[0].hauteur, maximum_ppm);
    fputs(buffer_string, fichier_sortie);
    //FIN DU HEADER
    
    //Ecriture des pixels
    for (size_t yp = 0; yp < patch->hauteur; yp++)  //Iteration sur les lignes de primitifs
    {
        for (size_t yi = 0; yi < triangles[0].hauteur; yi++)  //Iteration sur les lignes de pixels dans les primitifs
        {
            for (size_t xp = 0; xp < patch->largeur; xp++)  //Iteration sur les colonnes de primitifs
            {
                size_t offset_pixel = 3 * yi * triangles[0].largeur;
                struct primitif prim = List_primitif_get(patch->primitifs, xp + yp * patch->largeur);
                char* ptr_pixel = NULL;
                if (prim.nature == CARRE)
                    ptr_pixel = carres[prim.orientation].donnees_pixel->ptrMemory;
                else if (prim.nature == TRIANGLE)
                    ptr_pixel = triangles[prim.orientation].donnees_pixel->ptrMemory;
		else
		    puts("Erreur primitif invalid");

                fwrite(ptr_pixel + offset_pixel, 1, triangles[0].largeur * 3, fichier_sortie);
            }
        }
    }

    //Libération ressources
    for (size_t x = 0; x < 4; x++)
    {
        List_char_delete(triangles[x].donnees_pixel);
        List_char_delete(carres[x].donnees_pixel);
    }
}
