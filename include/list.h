
#include <stdlib.h>
#include <string.h>

typedef unsigned int uint_32;

/* Module list.h: gère des tableaux redimensionnables. Voir le document Documentation_list.h.txt pour plus de détails */

/* Règles d'utilisation:
 * Les fonctions utilisables en dehors du module sont new, delete, new_from_mem,
 * get, set, resize, size, push_back, insert, remove.
 * Pour la structure List_type, size et capacity peuvent être lus, mais non pas réécrits.
 * Enfin, il est possible d'accéder manuellement à la zone pointée par ptrMemory,
 * mais il ne faut pas changer ptrMemory lui-même.
 *
 * Pour créer un nouveau type de liste, il faut appeler la macro create_list avec un nom de
 * type SANS ESPACE (si nécessaire utiliser des typedef).
 * Exemple: "create_list(int);" générera la structure List_int, ainsi que les
 * fonctions List_int_new, List_int_push_back, etc...
 * Ne pas oublier d'appeler List_TYPE_delete pour libérer la mémoire
 */
#define create_list(TYPELIST) \
\
static const double ExtraMemoryFactor##TYPELIST = 1.8; \
static const int DefaultCapacity##TYPELIST = 10; \
\
typedef struct List_##TYPELIST { \
    TYPELIST* ptrMemory; /*Ne pas repointer vers une autre zone mémoire*/  \
    size_t size;  /*Ne pas modifier directement*/ \
    size_t capacity; /*Ne pas modifier directement*/ \
} List_##TYPELIST; \
 \
static inline void List_##TYPELIST##_realloc_more(List_##TYPELIST* list) \
{ \
    list->ptrMemory = realloc(list->ptrMemory, list->capacity * ExtraMemoryFactor##TYPELIST * sizeof(TYPELIST)); \
    list->capacity = list->capacity * ExtraMemoryFactor##TYPELIST; \
} \
 \
static inline List_##TYPELIST* List_##TYPELIST##_new_from_mem(TYPELIST const* mem, size_t size) \
{ \
    List_##TYPELIST* list = malloc(sizeof(List_##TYPELIST)); \
    list->size = size; \
    list->capacity = size * ExtraMemoryFactor##TYPELIST; \
    list->ptrMemory = malloc(list->capacity * sizeof(TYPELIST)); \
    memcpy(list->ptrMemory, mem, size * sizeof(TYPELIST)); \
    return list; \
} \
 \
static inline void List_##TYPELIST##_delete(List_##TYPELIST* list) \
{ \
    free(list->ptrMemory); \
    free(list); \
} \
 \
static inline TYPELIST List_##TYPELIST##_get(List_##TYPELIST* list, uint_32 position) \
{ \
    return list->ptrMemory[position]; \
} \
 \
static inline void List_##TYPELIST##_set(List_##TYPELIST* list, uint_32 position, TYPELIST item) \
{ \
    list->ptrMemory[position] = item; \
} \
 \
static inline size_t List_##TYPELIST##_size(List_##TYPELIST* list) \
{ \
    return list->size; \
} \
 \
static inline void List_##TYPELIST##_push_back(List_##TYPELIST* list, TYPELIST item) \
{ \
    list->size++; \
    list->ptrMemory[list->size - 1] = item; \
    if (list->size >= list->capacity) \
    { \
        List_##TYPELIST##_realloc_more(list); \
    } \
} \
 \
static inline void List_##TYPELIST##_insert(List_##TYPELIST* list, uint_32 position, TYPELIST item) \
{ \
    if (list->size + 1 >= list->capacity) \
    { \
        List_##TYPELIST##_realloc_more(list); \
    } \
    list->size++; \
    memmove(list->ptrMemory + position + 1, list->ptrMemory + position, sizeof(TYPELIST) * (list->size - 1 - position)); \
    List_##TYPELIST##_set(list, position, item); \
} \
 \
static inline void List_##TYPELIST##_remove(List_##TYPELIST* list, uint_32 position) \
{ \
    if (position >= list->size) \
    { \
        list->size--; \
        memmove(list->ptrMemory + position, list->ptrMemory + position + 1, sizeof(TYPELIST) * (list->size - position)); \
    } \
} \
 \
static inline void List_##TYPELIST##_resize(List_##TYPELIST* list, size_t new_size, TYPELIST defaultValue) \
{ \
    if (new_size == list->size) \
    { \
        return; \
    } \
    list->ptrMemory = realloc(list->ptrMemory, sizeof(TYPELIST) * new_size * ExtraMemoryFactor##TYPELIST); \
    if (list->size < new_size) \
    { \
        for (size_t i = list->size; i < new_size; i++) \
        { \
            list->ptrMemory[i] = defaultValue; \
        } \
    } \
    list->size = new_size; \
}\
static inline List_##TYPELIST* List_##TYPELIST##_new() \
{ \
    /*Empecher clang de se plaindre si les fonctions ne sont pas utilisées */ \
    (void)List_##TYPELIST##_new_from_mem;\
    (void)List_##TYPELIST##_resize;\
    (void)List_##TYPELIST##_insert;\
    (void)List_##TYPELIST##_push_back;\
    (void)List_##TYPELIST##_get;\
    (void)List_##TYPELIST##_set;\
    (void)List_##TYPELIST##_delete;\
    (void)List_##TYPELIST##_realloc_more;\
    (void)List_##TYPELIST##_remove;\
    List_##TYPELIST* list = malloc(sizeof(List_##TYPELIST)); \
    list->size = 0; \
    list->capacity = DefaultCapacity##TYPELIST; \
    list->ptrMemory = malloc(list->capacity * sizeof(TYPELIST)); \
    return list; \
}


//end of create_list
