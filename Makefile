##### Fonctions

#Trouver, récursivement dans un dossier, tous les fichiers dont le nom match un pattern
rec_wildcard = $(wildcard $(1)$(2)) $(foreach d,$(wildcard $(1)*), $(call rec_wildcard,$(d)/,$(2)))

#Trouver tous les noms de tous les sous-dossiers d'un dossier
subdirs = $(sort $(dir $(call rec_wildcard,$(1)/,*)))

##Deux targets: debug (pour débogage/valgrind) et release (exécutable optimisé)
##Lancer make sans target construira les deux targets debug et release automatiquement

SRCDIR := src
INCDIR := include
CC := clang
INC := -I./$(INCDIR)
LDLIBS := libparser.a
CCFLAGS := -std=c11 -Wall -Wextra -Werror -pedantic $(INC)
RFLAGS := -O2 -march=native
DFLAGS := -g
DEBUG_DIR = debug
RELEASE_DIR = release
SRCSUBDIRS := $(call subdirs,$(SRCDIR))
DEPSDIR := .deps
DEPSDIRS := $(DEPSDIR) $(addprefix  .deps/$(DEBUG_DIR)/, $(SRCSUBDIRS))   $(addprefix .deps/$(RELEASE_DIR)/, $(SRCSUBDIRS))
OBJDIRS := obj $(addprefix  obj/$(DEBUG_DIR)/, $(SRCSUBDIRS)) $(addprefix obj/$(RELEASE_DIR)/, $(SRCSUBDIRS))
BINDIRS := bin bin/$(DEBUG_DIR) bin/$(RELEASE_DIR)

EXEC := Patchwork
LNEXEC := $(EXEC)

SRC := $(call rec_wildcard,$(SRCDIR)/,*.c)
ASM := $(call rec_wildcard,$(SRCDIR)/,*.s)
DOBJS := $(SRC:$(SRCDIR)/%.c=obj/$(DEBUG_DIR)/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/$(DEBUG_DIR)/$(SRCDIR)/%.o)
ROBJS := $(SRC:$(SRCDIR)/%.c=obj/$(RELEASE_DIR)/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/$(RELEASE_DIR)/$(SRCDIR)/%.o)
OBJS := $(DOBJS) $(ROBJS)
# Fichiers de dépendances, pour savoir quels fichiers .c doivent être recompilés quand un header est changé
DDEPS := $(SRC:$(SRCDIR)/%.c=.deps/$(DEBUG_DIR)/$(SRCDIR)/%.c.d)
RDEPS := $(SRC:$(SRCDIR)/%.c=.deps/$(RELEASE_DIR)/$(SRCDIR)/%.c.d)
DEPS := $(DDEPS) $(RDEPS)
OUTASM := $(SRC:$(SRCDIR)/%.c=obj/$(RELEASE_DIR)/%.s) $(SRC:%.c=obj/$(DEBUG_DIR)/%.s)
I := $(SRC:%.c=obj/$(RELEASE_DIR)/%.i) $(SRC:%.c=obj/$(DEBUG_DIR)/%.i)

.PHONY: all rmlnexec debug release
all: setup rmlnexec debug release

-include $(DEPS)

debug: bin/$(DEBUG_DIR)/$(EXEC)
release: bin/$(RELEASE_DIR)/$(EXEC)

bin/$(DEBUG_DIR)/$(EXEC): $(DOBJS)
	$(CC) -o $@ $^ $(LDLIBS)

bin/$(RELEASE_DIR)/$(EXEC): $(ROBJS)
	$(CC) -o $@ $^ -s $(LDLIBS)
	ln -s $@ $(LNEXEC)

obj/$(DEBUG_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CCFLAGS) $(DFLAGS) -MM -MF .deps/$(DEBUG_DIR)/$<.d -MT "$@" -MP -c $<
	$(CC) $(CCFLAGS) $(DFLAGS) -c $< -o $@

obj/$(RELEASE_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CCFLAGS) $(RFLAGS) -MM -MF .deps/$(RELEASE_DIR)/$<.d -MT "$@" -MP -c $<
	$(CC) $(CCFLAGS) $(RFLAGS) -c $< -o $@

obj/$(DEBUG_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CC) $(CCFLAGS) $(DFLAGS) -c $< -o $@

obj/$(RELEASE_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CC) $(CCFLAGS) $(RFLAGS) -c $< -o $@

.PHONY: setup
setup: 
	@mkdir -p $(DEPSDIRS) $(BINDIRS) $(OBJDIRS)

.PHONY: clean
clean:
	rm -f $(OBJS) bin/$(DEBUG_DIR)/$(EXEC) bin/$(RELEASE_DIR)/$(EXEC) $(OUTASM) $(I) $(DEPS) $(LNEXEC)

.PHONY: rmlnexec
rmlnexec:
	@rm -f $(LNEXEC)
